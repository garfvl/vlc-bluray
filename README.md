Read Blurays on Microsoft Windows with VLC and MakeMKV
======================================================

## Tested on

* VLC 3.0 RC5
* MakeMKV Beta 1.10.8

## Download MakeMKV on the official website

[http://www.makemkv.com/](http://www.makemkv.com/)

![MakeMKV Website](img/makemkv-website.png "http://www.makemkv.com/")

## Install it on your preferred directory location (Note the install directory!)

![MakeMKV Install](img/makemkv-install.png "Install MakeMKV")

## Activate the MakeMKV license

There are two methods:

### Purchase a license and register it
![MakeMKV Purchase](img/makemkv-register.png "Register MakeMKV")


**or**


### Activate the included evaluation license by launching MakeMKV software and using it one time.
![MakeMKV First Use](img/makemkv-use1.png "Use MakeMKV one time")


## Get VLC 3.0

* **Release Candidate** for prudent people: Choose the last `vlc-3.0.0-rcX` in [http://download.videolan.org/testing/](http://download.videolan.org/testing/)
* **nightlies** for adventurers: [https://nightlies.videolan.org/build/win32/last/](https://nightlies.videolan.org/build/win32/last/)


## Copy the following library `libmmbd.dll` file from the MakeMKV install directory to **VLC directory**

* for VLC 32bit (win32) version: `libmmbd.dll`
* for VLC 64bit (win64) version: `libmmbd64.dll`

![MakeMKV library copy](img/makemkv-libcopy.png "Copy library from MakeMKV directory")

![MakeMKV library paste](img/makemkv-libcopy-dest.png "Paste library to VLC directory")

## VLC 64bit only: rename the `libmmbd` DLL

Rename the freshly copied `libmmbd64.dll` to `libmmbd.dll` (remove the `64` digits).

## Launch VLC and select The Bluray disc

![VLC select disc](img/vlc-select-disk.png "Select disc in VLC")


then


![VLC select bluray](img/vlc-select-bd.png "Select bluray in VLC")

## Voilà !

## Note on menus & java

You need `java jre` installed on your system to have the bluray menus supported on VLC.

**BUT**, if you have a Windows **64bit** version, you need to install `java jre` **64bit**, and not the standard (32bit) version.

Link to the `64bit java jre`: https://www.java.com/en/download/windows-64bit.jsp

